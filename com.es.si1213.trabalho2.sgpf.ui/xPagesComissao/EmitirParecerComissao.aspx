﻿<%@ Page Title="SGPF" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="EmitirParecerComissao.aspx.cs" Inherits="com.es.si1213.trabalho2.sgpf.ui._EmitirParecerComissao" %>

<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="st" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="~\xControls\DataFilter.ascx" TagName="DataFilter" TagPrefix="uc1" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Emissão de Parecer sobre o Projecto
    </h2>
    <h3>
        Selecione o Projecto:
    </h3>
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <%--TODO: Fix DataFilter--%><uc1:DataFilter ID="DataFilter1" runat="server" />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div style="overflow-x: auto; width: 100%; overflow-y: auto; height: 100%">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" EmptyDataText="Não existem projectos carentes de parecer"
                        DataKeyNames="NrProjecto" GridLines="None" AllowPaging="true" CssClass="mGrid"
                        PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" PageSize="5" OnPageIndexChanging="GridView1_PageIndexChanging"
                        AutoGenerateSelectButton="True">
                        <AlternatingRowStyle CssClass="alt" />
                        <Columns>
                            <asp:BoundField DataField="NrProjecto" HeaderText="Nº Projecto" ReadOnly="True" SortExpression="NrProjecto" />
                            <asp:BoundField DataField="NomeProjecto" HeaderText="Nome Projecto" SortExpression="NomeProjecto" />
                            <asp:BoundField DataField="MontanteSolicitado" HeaderText="Montante" SortExpression="MontanteSolicitado"
                                DataFormatString="{0:C}" />
                            <asp:BoundField DataField="AutorProjecto" HeaderText="Autor" SortExpression="AutorProjecto" />
                            <%--TODO: Field Status type/formating?--%>
                            <asp:BoundField DataField="StatusAprovacaoTecnico" HeaderText="Parecer Técnico" SortExpression="StatusAprovacaoTecnico" />
                            <asp:BoundField DataField="DataParecerTecnico" HeaderText="Data Parecer Técnico"
                                SortExpression="DataParecerTecnico" DataFormatString=" {0:d}" />
                            <asp:BoundField DataField="ObservacoesTecnico" HeaderText="Observações Técnico"
                                SortExpression="ObservacoesTecnico" />
                            <asp:BoundField DataField="UsernameTecnico" HeaderText="Utilizador Tecnico" SortExpression="UsernameTecnico" />
                            <%--TODO: Field Status type/formating?--%>
                            <asp:BoundField DataField="StatusAprovGestorFinanciamento" HeaderText="Parecer Gestor"
                                SortExpression="StatusAprovGestorFinanciamento" />
                            <asp:BoundField DataField="DataParecerGestorFinanciamento" HeaderText="Data Parecer Gestor"
                                SortExpression="DataParecerGestorFinanciamento" DataFormatString=" {0:d}" />
                            <asp:BoundField DataField="UsernameGestorFinanciamento" HeaderText="Utilizador Gestor"
                                SortExpression="UsernameGestorFinanciamento" />
                            <asp:BoundField DataField="ObservacoesGestorFinanciamento" HeaderText="Observações Gestor"
                                SortExpression="ObservacoesGestorFinanciamento" />
                        </Columns>
                        <PagerStyle CssClass="pgr" />
                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <table style="vertical-align: top">
            <tr>
                <td rowspan="2" style="vertical-align: top">
                    <p>
                        <asp:Label ID="lblParecer" runat="server" AssociatedControlID="rdBtParecer">Parecer:</asp:Label>
                        <asp:RadioButtonList ID="rdBtParecer" runat="server" OnSelectedIndexChanged="rdBtParecer_SelectedIndexChanged" AutoPostBack = "true">
                            <asp:ListItem>Favorável</asp:ListItem>
                            <asp:ListItem Selected="True">Não favorável</asp:ListItem>
                        </asp:RadioButtonList>
                    </p>
                </td>
                <td rowspan="2" style="vertical-align: top">
                    <p>
                        <asp:Label ID="lblTipoProjecto" runat="server" AssociatedControlID="rdTipoProjecto"
                            Visible="false">Tipo Projecto:</asp:Label>
                        <asp:RadioButtonList ID="rdTipoProjecto" runat="server" Visible="false" OnSelectedIndexChanged="rdTipoProjecto_SelectedIndexChanged" AutoPostBack = "true">
                            <asp:ListItem>Incentivos</asp:ListItem>
                            <asp:ListItem>Bonificações</asp:ListItem>
                        </asp:RadioButtonList>
                    </p>
                </td>
                <td style="vertical-align: top">
                    <p>
                        <asp:Label Visible="false" ID="lblInctNumPrestacoes" runat="server" AssociatedControlID="txtInctNumPrestacoes">Número de Prestações:</asp:Label>
                        <asp:TextBox Visible="false" ID="txtInctNumPrestacoes" runat="server" CssClass="textEntry">0</asp:TextBox>
                        <asp:CompareValidator Visible="false" CssClass="failureNotification" ID="CompareValidator1"
                            runat="server" ControlToValidate="txtInctNumPrestacoes" Operator="DataTypeCheck"
                            Type="Integer">
                                Entry must be Integer.
                        </asp:CompareValidator>
                        <asp:Label  Visible="false" ID="lblPeriodoBonifDe" runat="server">Periodo de Bonificação de:</asp:Label>
                    </p>
                </td>
                <td style="vertical-align: top">
                    <asp:Calendar  Visible="false" ID="cldPeriodoBonifDe" runat="server" Height="19px" Width="61px"></asp:Calendar>
                </td>
                <td style="vertical-align: top">
                    <asp:Label  Visible="false" ID="lblPeriodoBonifAte" runat="server">até:</asp:Label>
                </td>
                <td style="vertical-align: top">
                    <asp:Calendar  Visible="false" ID="cldPeriodoBonifAte" runat="server" Height="76px" Width="36px"></asp:Calendar>
                </td>
                <td style="vertical-align: top">
                    <p>
                        <asp:Label  Visible="false" ID="lblTaxa" runat="server">Taxa:</asp:Label>
                        <st:PercentBox  Visible="false" ID="prctTxtTaxa" runat="server" CssClass="textEntry" />
                    </p>
                    <p>
                        <asp:Label Visible="false" ID="lblMontanteMax" runat="server">Montante Máximo:</asp:Label>
                        <st:CurrencyBox Visible="false" ID="currTxtMontanteMax" runat="server" CssClass="textEntry" />
                    </p>
                    <p>
                        <asp:Label  Visible="false" ID="lblIdentContBank" runat="server">Conta Bancária:</asp:Label>
                        <asp:TextBox  Visible="false" ID="txtIdentContBank" runat="server" CssClass="textEntry" 
                            Width="196px"></asp:TextBox>
                    </p>
                </td>
            </tr>
        </table>
        <p>
            <%--Botão--%>
            <asp:Button ID="btnEmitirParecer" runat="server" Text="Submeter" class="submitButton"
                OnClick="btnEmitirParecer_click" />
            <span class="failureNotification">
                <asp:Literal ID="FailureText" runat="server"></asp:Literal>
            </span>
        </p>
        <%--Observações--%>
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblObservacoes" runat="server">Observações:</asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtObservacoes" AcceptsTab="true" AcceptsReturn="true" ScrollBars="ScrollBars.Vertical"
                        TextMode="multiline" runat="server" CssClass="bigTextEntry" Height="80px" Width="300px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
