﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.es.si1213.trabalho2.sgpf.ui.xControls;
using System.Web.Security;
using System.Diagnostics;
using System.Globalization;

namespace com.es.si1213.trabalho2.sgpf.ui
{
    public partial class _EmitirParecerComissao : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GridView1.DataSource = data.EmitirParecerComissaoProjectos.getListaDeProjectosParaParecerComissaoProjecto(Roles.GetAllRoles()[0], User.Identity.Name);
            GridView1.DataBind();
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DataFilter1.BeginFilter();
            }
        }

        void DataFilter1_OnFilterAdded()
        {
            DataFilter1.FilterSessionID = "EmitirParecer.aspx";
            DataFilter1.FilterDataSource();
            GridView1.DataBind();
        }

        protected void btnEmitirParecer_click(object sender, EventArgs e)
        {
            this.FailureText.Visible = false;
            if (GridView1.SelectedIndex == -1)
            {
                this.FailureText.Visible = true;
                this.FailureText.Text = "Selecione um Projecto por favor.";
                return;
            }

            Int32 NrProjecto = (Int32)GridView1.SelectedDataKey.Value;
            string observacoes = this.txtObservacoes.Text;

            bool parecerFavoravel = false;
            bool projectoIncentivos = false;
            int numPrestacoes = -1;
            decimal taxa = -1;
            decimal montanteMax = -1;
            string contaBancID = null;

            DateTime PeriodoBonifDe = DateTime.Now;
            DateTime PeriodoBonifAte = DateTime.Now;

            Debug.Assert(this.rdBtParecer.SelectedIndex == 0 || this.rdBtParecer.SelectedIndex == 1);//TODO: remover antes de passagem para PROD
            if (this.rdBtParecer.SelectedIndex == 0)//Favoravel
            {
                parecerFavoravel = true;
                if (this.rdTipoProjecto.SelectedIndex == 0)//Incentivos
                {
                    projectoIncentivos = true;
                    numPrestacoes = Int32.Parse(txtInctNumPrestacoes.Text);
                }
                else if (this.rdTipoProjecto.SelectedIndex == 1)//Bonificações
                {
                    PeriodoBonifDe = this.cldPeriodoBonifDe.SelectedDate;
                    PeriodoBonifAte = this.cldPeriodoBonifAte.SelectedDate;
                    if (DateTime.Compare(PeriodoBonifDe, PeriodoBonifAte) > 0)
                    {
                        this.FailureText.Visible = true;
                        this.FailureText.Text = "Datas incompatíveis.";
                        return;
                    }
                    taxa = decimal.Parse(this.prctTxtTaxa.Text.Replace(System.Globalization.CultureInfo.CurrentCulture.NumberFormat.PercentSymbol, ""));
                    montanteMax = decimal.Parse(this.currTxtMontanteMax.Text, NumberStyles.Currency);
                    contaBancID = this.txtIdentContBank.Text;
                }
                else
                {
                    this.FailureText.Visible = true;
                    this.FailureText.Text = "Selecione um Tipo de Projecto por favor.";
                    return;
                }

            }
                                                                     
            data.EmitirParecerComissaoProjectos.parecerComissaoProjecto(NrProjecto, User.Identity.Name, parecerFavoravel, 
                observacoes, projectoIncentivos, PeriodoBonifDe, PeriodoBonifAte, numPrestacoes, taxa, montanteMax, contaBancID);

            hideAll();
            rdBtParecer.SelectedIndex = 1;


            
            Page_Load(null, EventArgs.Empty); // Provoca um Refresh á Gridview
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }

        protected void rdBtParecer_SelectedIndexChanged(object sender, EventArgs e)
        {//idx = 0 -> Favorável; idx = 1 -> Não favorável
            Debug.Assert(this.rdBtParecer.SelectedIndex == 0 || this.rdBtParecer.SelectedIndex == 1);//TODO: remover antes de passagem para PROD

            if (this.rdBtParecer.SelectedIndex == 0)
                showFavoravel();
            else
                hideAll();
        }

        private void hideAll()
        {
            this.lblTipoProjecto.Visible = false;
            this.rdTipoProjecto.Visible = false;

            this.lblInctNumPrestacoes.Visible = false;
            this.txtInctNumPrestacoes.Visible = false;

            this.lblPeriodoBonifDe.Visible = false;
            this.cldPeriodoBonifDe.Visible = false;
            this.lblPeriodoBonifAte.Visible = false;
            this.cldPeriodoBonifAte.Visible = false;

            this.lblTaxa.Visible = false;
            this.prctTxtTaxa.Visible = false;

            this.lblMontanteMax.Visible = false;
            this.currTxtMontanteMax.Visible = false;

            this.lblIdentContBank.Visible = false;
            this.txtIdentContBank.Visible = false;
        }

        private void showFavoravel()
        {
            this.lblTipoProjecto.Visible = true;
            this.rdTipoProjecto.Visible = true;
        }

        protected void rdTipoProjecto_SelectedIndexChanged(object sender, EventArgs e)
        {//idx = 0 -> Incentivos; idx = 1 -> Bonificações
            Debug.Assert(this.rdTipoProjecto.SelectedIndex == 0 || this.rdTipoProjecto.SelectedIndex == 1);//TODO: remover antes de passagem para PROD

            if (this.rdTipoProjecto.SelectedIndex == 0)//Incentivos
            {
                this.lblPeriodoBonifDe.Visible = false;
                this.cldPeriodoBonifDe.Visible = false;
                this.lblPeriodoBonifAte.Visible = false;
                this.cldPeriodoBonifAte.Visible = false;

                this.lblTaxa.Visible = false;
                this.prctTxtTaxa.Visible = false;

                this.lblMontanteMax.Visible = false;
                this.currTxtMontanteMax.Visible = false;

                this.lblIdentContBank.Visible = false;
                this.txtIdentContBank.Visible = false;

                this.lblInctNumPrestacoes.Visible = true;
                this.txtInctNumPrestacoes.Visible = true;
            }
            else//Bonificações
            {
                this.lblInctNumPrestacoes.Visible = false;
                this.txtInctNumPrestacoes.Visible = false;

                this.lblPeriodoBonifDe.Visible = true;
                this.cldPeriodoBonifDe.Visible = true;
                this.lblPeriodoBonifAte.Visible = true;
                this.cldPeriodoBonifAte.Visible = true;

                this.lblTaxa.Visible = true;
                this.prctTxtTaxa.Visible = true;

                this.lblMontanteMax.Visible = true;
                this.currTxtMontanteMax.Visible = true;

                this.lblIdentContBank.Visible = true;
                this.txtIdentContBank.Visible = true;

            }
        }
    }
}
