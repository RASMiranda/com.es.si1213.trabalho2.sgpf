﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using com.es.si1213.trabalho2.sgpf.business;
using com.es.si1213.trabalho2.sgpf.ui.xControls;

namespace com.es.si1213.trabalho2.sgpf.ui
{
    public partial class _SuspenderProjecto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GridView1.DataSource = SuspenderProjecto.getListaDeProjectosParaSuspender(Roles.GetAllRoles()[0], User.Identity.Name);
            GridView1.DataBind();
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DataFilter1.BeginFilter();
            }
        }

        void DataFilter1_OnFilterAdded()
        {
            DataFilter1.FilterSessionID = "SuspenderProjecto.aspx";
            DataFilter1.FilterDataSource();
            GridView1.DataBind();
        }

        protected void btnSuspender_click(object sender, EventArgs e)
        {
            this.FailureText.Visible = false;
            if (GridView1.SelectedIndex == -1)
            {
                this.FailureText.Visible = true;
                this.FailureText.Text = "Selecione um Projecto por favor.";
                return;
            }

            Int32 NrProjecto = (Int32)GridView1.SelectedDataKey.Value;
            string observacoes = this.txtObservacoes.Text;

            SuspenderProjecto.suspenderProjecto(NrProjecto, User.Identity.Name, observacoes);

            this.txtObservacoes.Text = "";

            // Mensagem de projecto Suspenso
            System.Web.HttpContext.Current.Response.Write("<script>alert('Projecto número " + NrProjecto.ToString() + " suspenso!');</script>");
            Page_Load(null, EventArgs.Empty); // Provoca um Refresh á Gridview
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }
    }
}
