﻿using System;
using System.Web.UI.WebControls;
using System.Web.Security;
using com.es.si1213.trabalho2.sgpf.business;


namespace com.es.si1213.trabalho2.sgpf.ui
{
    public partial class _ArquivarProjecto : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            GridView1.DataSource = ArquivarProjecto.getListaDeProjectosParaArquivar(Roles.GetAllRoles()[0], User.Identity.Name);
            GridView1.DataBind();
        }

 

        protected void btnArquivar_click(object sender, EventArgs e)
        {
            //TODO: Try/Catch
            this.FailureText.Visible = false;
            if (GridView1.SelectedIndex == -1)
            {
                this.FailureText.Visible = true;
                this.FailureText.Text = "Selecione um Projecto por favor.";
                return;
            }

            Int32 NrProjecto = (Int32)GridView1.SelectedDataKey.Value;
            string Observacoes = this.txtObservacoes.Text;

            ArquivarProjecto.arquivarProjecto(NrProjecto, User.Identity.Name, Observacoes);
            this.txtObservacoes.Text = "";

            // Mensagem de projecto Arquivado
            System.Web.HttpContext.Current.Response.Write("<script>alert('Projecto número " + NrProjecto.ToString() + " arquivado!');</script>");
            Page_Load(null, EventArgs.Empty); // Provoca um Refresh á Gridview
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }
    }
}
