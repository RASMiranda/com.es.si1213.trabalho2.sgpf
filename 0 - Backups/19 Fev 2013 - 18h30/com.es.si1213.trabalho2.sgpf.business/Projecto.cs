﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace com.es.si1213.trabalho2.sgpf.business
{


    public class Projecto : Settings
    {
        private int numProjecto;

        public int NumProjecto
        {
            get { return numProjecto; }
            set { numProjecto = value; }
        }
        private string nomeProjecto;

        public string NomeProjecto
        {
            get { return nomeProjecto; }
            set { nomeProjecto = value; }
        }
        private decimal montanteSolicitado;

        public decimal MontanteSolicitado
        {
            get { return montanteSolicitado; }
            set { montanteSolicitado = value; }
        }
        private string solicitadorProjecto;

        public string SolicitadorProjecto
        {
            get { return solicitadorProjecto; }
            set { solicitadorProjecto = value; }
        }
        private DateTime dataAbertura;

        public DateTime DataAbertura
        {
            get { return dataAbertura; }
            set { dataAbertura = value; }
        }
        private Boolean parecerTecnicoFavoravel;

        public Boolean ParecerTecnicoFavoravel
        {
            get { return parecerTecnicoFavoravel; }
            set { parecerTecnicoFavoravel = value; }
        }
        private string usernameTecnico;

        public string UsernameTecnico
        {
            get { return usernameTecnico; }
            set { usernameTecnico = value; }
        }


        //// Obtém o próximo número de Projecto disponível
        //public static int ObtemProxNrProjectoDisponivel()
        //{
        //    int nrProjecto = 0;
        //    SqlCommand commandObtemNrUltimoProj = new SqlCommand(SQLQueryObtemNrUltimoProjecto, connection);
        //    commandObtemNrUltimoProj.CommandType = CommandType.Text;

        //    try
        //    {
        //        nrProjecto = ((int)commandObtemNrUltimoProj.ExecuteScalar() + 1);
        //    }
        //    catch (Exception)
        //    {
        //        System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de acesso à base de dados');</script>");
        //    }

        //    commandObtemNrUltimoProj.Dispose();

        //    return nrProjecto;
        //}



        //// Abre novo Projecto
        //public static string AbrirNovoProjecto(string param2, decimal param3, string param4, bool param5, string param6, string param7)
        //{
        //    int counter = 0;

        //    connection = new SqlConnection(GetConnectionString());
        //    SqlCommand commandAbreProj = new SqlCommand(SQLQueryAbreProjecto, connection);

        //    try
        //    {
        //        using (connection)
        //        {
        //            connection.Open();
        //            commandAbreProj.CommandType = CommandType.Text;

        //            counter = ObtemProxNrProjectoDisponivel();

        //            commandAbreProj.Parameters.AddWithValue("@Item1", counter);
        //            commandAbreProj.Parameters.AddWithValue("@Item2", param2);
        //            commandAbreProj.Parameters.AddWithValue("@Item3", param3);
        //            commandAbreProj.Parameters.AddWithValue("@Item4", param4);
        //            commandAbreProj.Parameters.AddWithValue("@Item5", param5);
        //            commandAbreProj.Parameters.AddWithValue("@Item6", param6);
        //            commandAbreProj.Parameters.AddWithValue("@Item7", param7);

        //            commandAbreProj.ExecuteNonQuery();
        //            commandAbreProj.Dispose();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
        //    }

        //    return counter.ToString();
        //}



        public static string Abrir(
                         string NomeProjecto
                        , decimal MontanteSolicitado
                        , string SolicitadorProjecto
                        , Boolean ParecerTecnicoFavoravel
                        , string UsernameTecnico
                        , string Observacoes)
        {

           
            //return (AbrirNovoProjecto(NomeProjecto
            return (AbrirProjecto.AbrirNovoProjecto(NomeProjecto
                         , MontanteSolicitado
                         , SolicitadorProjecto
                         , ParecerTecnicoFavoravel
                         , UsernameTecnico
                         , Observacoes));
        }
   

        //public static object getListaDeProjectosParaArquivar(string Papel, string Username)
        //{
        //    return data.Projecto.getListaDeProjectosParaArquivar(Papel, Username);
        //}

        
        //public static void arquivarProjecto(int NrProjecto, string Username, string observacoes)
        //{
        //    string param1 = "@Numero";
        //    string param2 = "@User";
        //    string param3 = "@Observacoes";
        //    string param4 = "@Data";
        //    int campoParam1 = NrProjecto;
        //    string campoParam2 = Username;
        //    string campoParam3 = observacoes;
        //    DateTime campoParam4 = DateTime.Today;

        //    connection = new SqlConnection(GetConnectionString());
        //    command = new SqlCommand(SQLQueryArquivaProjecto, connection);

        //    try
        //    {
        //        using (connection)
        //        {
        //            connection.Open();

        //            command.CommandType = CommandType.Text;
        //            command.Parameters.AddWithValue(param1, campoParam1);
        //            command.Parameters.AddWithValue(param2, campoParam2);
        //            command.Parameters.AddWithValue(param3, campoParam3);
        //            command.Parameters.AddWithValue(param4, campoParam4);
        //            command.ExecuteNonQuery();
        //        }

        //        command.Dispose();
        //    }
        //    catch (Exception)
        //    {
        //        System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
        //    }

        //}


        //public static object getListaDeProjectosParaReactivar()
        //{

        //    connection = new SqlConnection(GetConnectionString());
        //    command = new SqlCommand(SQLQueryListaProjectosParaReactivar, connection);
        //    dt = new DataTable();
           
        //    try
        //    {
        //        using (connection)
        //        {
        //            connection.Open();
        //            command.CommandType = CommandType.Text;
        //            dt = data.AccessData.ExecuteReader(command);
        //        }
                
        //        command.Dispose();
        //    }

            
        //    catch (Exception)
        //    {
        //        System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
        //    }

        //    return dt;    
        //}

        //public static void reactivarProjecto(int NrProjecto, string Username, string observacoes)//, string Papel
        //{

        //    string param1 = "@Numero";
        //    string param2 = "@User";
        //    string param3 = "@Observacoes";
            
        //    int campoParam1 = NrProjecto;
        //    string campoParam2 = Username;
        //    string campoParam3 = observacoes;

        //    connection = new SqlConnection(GetConnectionString());
        //    command = new SqlCommand(SQLQueryReactivarProjecto, connection);

        //    try
        //    {
        //        using (connection)
        //        {
        //            connection.Open();

        //            command.CommandType = CommandType.Text;
        //            command.Parameters.AddWithValue(param1, campoParam1);
        //            command.Parameters.AddWithValue(param2, campoParam2);
        //            command.Parameters.AddWithValue(param3, campoParam3);
        //            command.ExecuteNonQuery();
        //        }

        //        command.Dispose();
        //    }
        //    catch (Exception)
        //    {
        //        System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
        //    }

        //}

        public static object getListaDeProjectosParaSuspender(string Papel, string Username)
        {
            //TODO.Miguel: business.Projecto.getListaDeProjectosParaSuspender
            throw new NotImplementedException();
        }

        public static void suspenderProjecto(int NrProjecto, string Username, string observacoes)
        {

            string param1 = "@Numero";
            string param2 = "@User";
            string param3 = "@Observacoes";

            int campoParam1 = NrProjecto;
            string campoParam2 = Username;
            string campoParam3 = observacoes;

            connection = new SqlConnection(GetConnectionString());
            command = new SqlCommand(SQLQuerySuspendeProjecto, connection);

            try
            {
                using (connection)
                {
                    connection.Open();

                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue(param1, campoParam1);
                    command.Parameters.AddWithValue(param2, campoParam2);
                    command.Parameters.AddWithValue(param3, campoParam3);
                    command.ExecuteNonQuery();
                }

                command.Dispose();
            }
            catch (Exception)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
            }

        }

        public static object getListaDeProjectosParecerGestor(string Username)
        {
            string param1 = "@User";
            string campoParam1 = Username;

            connection = new SqlConnection(GetConnectionString());
            command = new SqlCommand(SQLQueryListaParecerGestor, connection);
            dt = new DataTable();

            try
            {
                using (connection)
                {
                    connection.Open();
                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue(param1, campoParam1);
                    dt = data.AccessData.ExecuteReader(command);
                }

                command.Dispose();
            }


            catch (Exception)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
            }

            return dt;
        }

        public static void parecerGestorProjecto(int NrProjecto, bool parecerFavoravel, string Username, string observacoes)
        {

            string param1 = "@Numero";
            string param2 = "@Parecer";
            string param3 = "@User";
            string param4 = "@Observacoes";

            int campoParam1 = NrProjecto;
            int campoParam2;
            string campoParam3 = Username;
            string campoParam4 = observacoes;
           
            campoParam2 = (parecerFavoravel.Equals(true)? campoParam2 = 1: campoParam2 = 0);

            connection = new SqlConnection(GetConnectionString());
            command = new SqlCommand(SQLQueryParecerGestorProjecto, connection);

            try
            {
                using (connection)
                {
                    connection.Open();

                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue(param1, campoParam1);
                    command.Parameters.AddWithValue(param2, campoParam2);
                    command.Parameters.AddWithValue(param3, campoParam3);
                    command.Parameters.AddWithValue(param4, campoParam4);
                    command.ExecuteNonQuery();
                }

                command.Dispose();
            }
            catch (Exception)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
            }

        }

        public static object getListaDeProjectosParecerComissao(string Username)
        {

            string param1 = "@User";
            string campoParam1 = Username;

            connection = new SqlConnection(GetConnectionString());
            command = new SqlCommand(SQLQueryListaParecerComissao, connection);
            dt = new DataTable();

            try
            {
                using (connection)
                {
                    connection.Open();
                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue(param1, campoParam1);
                    dt = data.AccessData.ExecuteReader(command);
                }

                command.Dispose();
            }


            catch (Exception)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
            }

            return dt;
        }

        public static void parecerComissaoProjecto(int NrProjecto, string Username, bool parecerFavoravel, string observacoes)
        {

            string param1 = "@Numero";
            string param2 = "@User";
            string param3 = "@Parecer";
            string param4 = "@Observacoes";

            int campoParam1 = NrProjecto;
            string campoParam2 = Username;
            int campoParam3;
            string campoParam4 = observacoes;

            campoParam3 = (parecerFavoravel.Equals(true) ? campoParam3 = 1 : campoParam3 = 0);

            connection = new SqlConnection(GetConnectionString());
            command = new SqlCommand(SQLQueryParecerComissaoFinanceira, connection);

            try
            {
                using (connection)
                {
                    connection.Open();

                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue(param1, campoParam1);
                    command.Parameters.AddWithValue(param2, campoParam2);
                    command.Parameters.AddWithValue(param3, campoParam3);
                    command.Parameters.AddWithValue(param4, campoParam4);
                    command.ExecuteNonQuery();
                }

                command.Dispose();
            }
            catch (Exception)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
            }
            
        }

        public static void reforcoProjecto(int NrProjecto, string Username, decimal Montante, string observacoes)
        {
            decimal montanteActual = 0;

            connection = new SqlConnection(GetConnectionString());
            command = new SqlCommand(SQLQueryObtemMontanteActualDeProjecto, connection);

            try
            {
                using (connection)
                {
                    connection.Open();
                    command.CommandType = CommandType.Text;
                    dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        montanteActual = (decimal)(dr["MontanteSolicitado"]);
                    }
                }

                command.Dispose();
            }


            catch (Exception)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
            }

            string param1 = "@Numero";
            string param2 = "@User";
            string param3 = "@NovoMontante";
            string param4 = "@Observacoes";


            int campoParam1 = NrProjecto;
            string campoParam2 = Username;
            decimal campoParam3 =  montanteActual + Montante;
            string campoParam4 = observacoes;

            connection = new SqlConnection(GetConnectionString());
            command = new SqlCommand(SQLQueryReforcoProjecto, connection);

            try
            {
                using (connection)
                {
                    connection.Open();

                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue(param1, campoParam1);
                    command.Parameters.AddWithValue(param2, campoParam2);
                    command.Parameters.AddWithValue(param3, campoParam3);
                    command.Parameters.AddWithValue(param4, campoParam4);
                    command.ExecuteNonQuery();
                }

                command.Dispose();
            }


            catch (Exception)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
            }

        }

        public static object getListaDeProjectosReforco(string Username)
        {

            string param1 = "@User";
            string campoParam1 = Username;

            connection = new SqlConnection(GetConnectionString());
            command = new SqlCommand(SQLQueryListaProjectosReforco, connection);
            dt = new DataTable();

            try
            {
                using (connection)
                {
                    connection.Open();
                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue(param1, campoParam1);
                    dt = data.AccessData.ExecuteReader(command);
                }

                command.Dispose();
            }


            catch (Exception)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
            }

            return dt;



        }

        public static void parecerComissaoProjecto(int NrProjecto, string Username, bool parecerFavoravel, string Obeservavoes, bool projectoIncentivos, DateTime PeriodoBonifDe, DateTime PeriodoBonifAte, int numPrestacoes, decimal taxa, decimal montanteMax, string contaBancID)
        {

           // Quem é a comissão de Projecto?
           // O que é para fazer aqui ?

            //TODO.Miguel: business.Projecto.parecerComissaoProjecto
            throw new NotImplementedException();
        }
    }
}
