﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.es.si1213.trabalho2.sgpf.ui.Account
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                // se vieram para a pagina directamente, ReturnUrl está a nulo
                if (String.IsNullOrEmpty(Request["ReturnUrl"]))
                {   
                    Response.Redirect("~/Default.aspx");
                }
                else //Caso contráro, vieram para a pagina após tentativa de acesso a uma página sem permissão
                {
                    Response.Redirect("~/AccessDenied.aspx");
                }
            }
            RegisterHyperLink.NavigateUrl = "Register.aspx?ReturnUrl=" + HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
        }
    }
}
