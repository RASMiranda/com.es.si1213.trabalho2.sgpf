﻿using System;
using System.Globalization;

namespace com.es.si1213.trabalho2.sgpf.ui
{
    public partial class _AbrirProjecto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnAbrirProjecto_click(object sender, EventArgs e)
        {
            //TODO: Try/Catch
            int NumProjecto;

            string NomeProjecto = this.txtNomeProjecto.Text;
            decimal MontanteSolicitado = decimal.Parse(this.currTxtMontante.Text, NumberStyles.Currency);
            string SolicitadorProjecto = this.txtSolicitadoPor.Text;
            string UsernameTecnico = User.Identity.Name;
            string Obeservavoes = this.txtObservacoes.Text;

            Boolean parecerFavoralvel = false;
            if(this.rdBtParecer.SelectedIndex == 0)
                parecerFavoralvel = true;
          
            txtNumProjecto.Text = business.Projecto.Abrir(NomeProjecto, MontanteSolicitado, SolicitadorProjecto, parecerFavoralvel, UsernameTecnico, Obeservavoes);
           

            // Efectua limpeza de campos
            txtNomeProjecto.Text = "";
            txtSolicitadoPor.Text = "";
            txtObservacoes.Text = "";
            System.Web.HttpContext.Current.Response.Write("<script>alert('Este Projecto tem o número: " + txtNumProjecto.Text + "');</script>");
            Server.Transfer("~/xPagesTecnico/DefaultTecnico.aspx");

        }
    }
}
