﻿<%@ Page Title="SGPF" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="SuspenderProjecto.aspx.cs" Inherits="com.es.si1213.trabalho2.sgpf.ui._SuspenderProjecto" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="~\xControls\DataFilter.ascx" TagName="DataFilter" TagPrefix="uc1" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Suspensão de Projecto
    </h2>
    <h3>
        Selecione o Projecto que pertende Suspender:
    </h3>
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <%--TODO: Fix DataFilter--%><uc1:DataFilter ID="DataFilter1" runat="server" />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div style="overflow-x: auto; width: 100%; overflow-y: auto; height: 100%">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" emptydatatext="Não existem projectos disponiveis para suspender"
                        DataKeyNames="NrProjecto" GridLines="None" AllowPaging="true" CssClass="mGrid"
                        PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" PageSize="5" OnPageIndexChanging="GridView1_PageIndexChanging"
                        AutoGenerateSelectButton="True">
                        <AlternatingRowStyle CssClass="alt" />
                        <Columns>
                            <asp:BoundField DataField="NrProjecto" HeaderText="Nº Projecto" ReadOnly="True" SortExpression="NrProjecto" />
                            <asp:BoundField DataField="NomeProjecto" HeaderText="Nome Projecto" SortExpression="NomeProjecto" />
                            <asp:BoundField DataField="MontanteSolicitado" HeaderText="Montante" SortExpression="MontanteSolicitado"
                                DataFormatString="{0:C}" />
                            <asp:BoundField DataField="AutorProjecto" HeaderText="Autor" SortExpression="AutorProjecto" />
                            <%--TODO: Field Status type/formating?--%>
                            <asp:BoundField DataField="StatusAprovacaoTecnico" HeaderText="Parecer Técnico" SortExpression="StatusAprovacaoTecnico" />
                            <asp:BoundField DataField="DataParecerTecnico" HeaderText="Data Parecer Técnico"
                                SortExpression="DataParecerTecnico" DataFormatString=" {0:d}" />
                            <asp:BoundField DataField="ObeservacoesTecnico" HeaderText="Obeservações Técnico"
                                SortExpression="ObeservacoesTecnico" />
                            <asp:BoundField DataField="UsernameTecnico" HeaderText="Utilizador Tecnico" SortExpression="UsernameTecnico" />
                            <%--TODO: Field Status type/formating?--%>
                            <asp:BoundField DataField="StatusAprovGestorFinanciamento" HeaderText="Parecer Gestor"
                                SortExpression="StatusAprovGestorFinanciamento" />
                            <asp:BoundField DataField="DataParecerGestorFinanciamento" HeaderText="Data Parecer Gestor"
                                SortExpression="DataParecerGestorFinanciamento" DataFormatString=" {0:d}" />
                            <asp:BoundField DataField="UsernameGestorFinanciamento" HeaderText="Utilizador Gestor"
                                SortExpression="UsernameGestorFinanciamento" />
                            <asp:BoundField DataField="ObeservacoesGestorFinanciamento" HeaderText="Obeservações Gestor"
                                SortExpression="ObeservacoesGestorFinanciamento" />
                            <%--TODO: Field Status type/formating?--%>
                            <asp:BoundField DataField="StatusAprovComissaoFinanceira" HeaderText="Parecer Comissão"
                                SortExpression="StatusAprovComissaoFinanceira" />
                            <asp:BoundField DataField="DataParecerComissaoFinanceira" HeaderText="Data Parecer Comissão"
                                SortExpression="DataParecerComissaoFinanceira" DataFormatString=" {0:d}" />
                            <asp:BoundField DataField="UsernameComissaoFinanceira" HeaderText="Utilizador Comissão"
                                SortExpression="UsernameComissaoFinanceira" />
                            <asp:BoundField DataField="ObeservacoesComissaoFinanceira" HeaderText="Obeservações Comissão"
                                SortExpression="ObeservacoesComissaoFinanceira" />
                            <asp:BoundField DataField="DataProjectoArquivado" HeaderText="Data Arquivo" SortExpression="DataProjectoArquivado"
                                DataFormatString=" {0:d}" />
                            <asp:BoundField DataField="PapelArquivador" HeaderText="Arquivado Por" SortExpression="PapelArquivador" />
                            <asp:BoundField DataField="UsernameArquivador" HeaderText="Utilizador" SortExpression="UsernameArquivador" />
                            <asp:BoundField DataField="ObeservacoesProjectoArquivado" HeaderText="Obeservações Arquivo" />
                        </Columns>
                        <PagerStyle CssClass="pgr" />
                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Button ID="btnSuspender" runat="server" Text="Suspender Projecto" class="submitButton"
            OnClick="btnSuspender_click" />
        <span class="failureNotification">
            <asp:Literal ID="FailureText" runat="server"></asp:Literal>
        </span>
        <%--Observações--%>
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblObservacoes" runat="server">Observações:</asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtObservacoes" AcceptsTab="true" AcceptsReturn="true" ScrollBars="ScrollBars.Vertical"
                        TextMode="multiline" runat="server" CssClass="bigTextEntry" Height="80px" Width="300px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
