﻿<%@ Page Title="SGPF" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="About.aspx.cs" Inherits="com.es.si1213.trabalho2.sgpf.ui.About" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Sobre
    </h2>
    <p>
        Sistema desenvolvido por <b>Grupo05</b>.
    </p>
    <p style="color: #C0C0C0">
        © Copyright 2013 - Organização XYZ. Todos os direitos reservados.
    </p>
</asp:Content>
