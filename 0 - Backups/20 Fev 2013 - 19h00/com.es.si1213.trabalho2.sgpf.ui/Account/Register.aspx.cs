﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace com.es.si1213.trabalho2.sgpf.ui.Account
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterUser.ContinueDestinationPageUrl = Request.QueryString["ReturnUrl"];
        }

        protected void RegisterUser_CreatedUser(object sender, EventArgs e)
        {
            //TODO: Try/Catch
            Roles.AddUserToRole(RegisterUser.UserName, ((DropDownList)RegisterUser.CreateUserStep.ContentTemplateContainer.FindControl("RoleDropDownList")).SelectedValue);
            
            FormsAuthentication.SetAuthCookie(RegisterUser.UserName, false /* createPersistentCookie */);

            string continueUrl = RegisterUser.ContinueDestinationPageUrl;
            if (String.IsNullOrEmpty(continueUrl))
            {
                continueUrl = "~/";
            }
            Response.Redirect(continueUrl);

        }

        protected void RoleDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}
