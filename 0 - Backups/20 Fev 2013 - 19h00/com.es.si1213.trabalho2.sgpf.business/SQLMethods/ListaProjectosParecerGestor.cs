﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace com.es.si1213.trabalho2.sgpf.business.SQLMethods
{
    class ListaProjectosParecerGestor : ConnectionUtils
    {

        public static object getListaDeProjectosParecerGestor(string Username)
        {
            string param1 = "@User";
            string campoParam1 = Username;

            connection = new SqlConnection(GetConnectionString());
            command = new SqlCommand(SQLQueryListaParecerGestor, connection);
            dt = new DataTable();

            try
            {
                using (connection)
                {
                    connection.Open();
                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue(param1, campoParam1);
                    dt = data.AccessData.ExecuteReader(command);
                }

                command.Dispose();
            }


            catch (Exception)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
            }

            return dt;
        }
    }
}
