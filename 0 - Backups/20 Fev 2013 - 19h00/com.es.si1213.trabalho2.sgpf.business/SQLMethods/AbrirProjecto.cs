﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace com.es.si1213.trabalho2.sgpf.business
{
    class AbrirProjecto : ConnectionUtils
    {

        // Obtém o próximo número de Projecto disponível
        public static int ObtemProxNrProjectoDisponivel()
        {
            int nrProjecto = 0;
            SqlCommand commandObtemNrUltimoProj = new SqlCommand(SQLQueryObtemNrUltimoProjecto, connection);
            commandObtemNrUltimoProj.CommandType = CommandType.Text;

            try
            {
                nrProjecto = ((int)commandObtemNrUltimoProj.ExecuteScalar() + 1);
            }
            catch (Exception)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de acesso à base de dados');</script>");
            }

            commandObtemNrUltimoProj.Dispose();

            return nrProjecto;
        }



        // Abre novo Projecto
        public static string AbrirNovoProjecto(string param2, decimal param3, string param4, bool param5, string param6, string param7, DateTime param8)
        {
            int counter = 0;

            connection = new SqlConnection(GetConnectionString());
            SqlCommand commandAbreProj = new SqlCommand(SQLQueryAbreProjecto, connection);

            try
            {
                using (connection)
                {
                    connection.Open();
                    commandAbreProj.CommandType = CommandType.Text;

                    counter = ObtemProxNrProjectoDisponivel();

                    commandAbreProj.Parameters.AddWithValue("@Item1", counter);
                    commandAbreProj.Parameters.AddWithValue("@Item2", param2);
                    commandAbreProj.Parameters.AddWithValue("@Item3", param3);
                    commandAbreProj.Parameters.AddWithValue("@Item4", param4);
                    commandAbreProj.Parameters.AddWithValue("@Item5", param5);
                    commandAbreProj.Parameters.AddWithValue("@Item6", param6);
                    commandAbreProj.Parameters.AddWithValue("@Item7", param7);
                    commandAbreProj.Parameters.AddWithValue("@Item8", param8);

                    commandAbreProj.ExecuteNonQuery();
                    commandAbreProj.Dispose();
                }

            }
            catch (Exception ex)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
            }

            return counter.ToString();
        }
    }
}
