﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace com.es.si1213.trabalho2.sgpf.business.SQLMethods
{
    public class ReactivarProjecto : ConnectionUtils
    {


        public static object getListaDeProjectosParaReactivar(string Papel, string Username)
        {
            return data.Projecto.getListaDeProjectosParaReactivar(Papel, Username);
        }



        public static void reactivarProjecto(int NrProjecto, string Username, string observacoes)//, string Papel
        {

            string param1 = "@Numero";
            string param2 = "@User";
            string param3 = "@Observacoes";

            int campoParam1 = NrProjecto;
            string campoParam2 = Username;
            string campoParam3 = observacoes;

            connection = new SqlConnection(GetConnectionString());
            command = new SqlCommand(SQLQueryReactivarProjecto, connection);

            try
            {
                using (connection)
                {
                    connection.Open();

                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue(param1, campoParam1);
                    command.Parameters.AddWithValue(param2, campoParam2);
                    command.Parameters.AddWithValue(param3, campoParam3);
                    command.ExecuteNonQuery();
                }

                command.Dispose();
            }
            catch (Exception)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
            }

        }
    }
}
