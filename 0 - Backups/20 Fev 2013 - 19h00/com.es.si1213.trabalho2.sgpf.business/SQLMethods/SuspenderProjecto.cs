﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace com.es.si1213.trabalho2.sgpf.business.SQLMethods
{
    public class SuspenderProjecto : ConnectionUtils
    {

        public static object getListaDeProjectosParaSuspender(string Papel, string Username)
        {
            return data.Projecto.getListaDeProjectosParaSuspender(Papel, Username);
        }

        public static void suspenderProjecto(int NrProjecto, string Username, string observacoes)
        {

            string param1 = "@Numero";
            string param2 = "@User";
            string param3 = "@Observacoes";

            int campoParam1 = NrProjecto;
            string campoParam2 = Username;
            string campoParam3 = observacoes;

            connection = new SqlConnection(GetConnectionString());
            command = new SqlCommand(SQLQuerySuspendeProjecto, connection);

            try
            {
                using (connection)
                {
                    connection.Open();

                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue(param1, campoParam1);
                    command.Parameters.AddWithValue(param2, campoParam2);
                    command.Parameters.AddWithValue(param3, campoParam3);
                    command.ExecuteNonQuery();
                }

                command.Dispose();
            }
            catch (Exception)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
            }

        }
    }
}
