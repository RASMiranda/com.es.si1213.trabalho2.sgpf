﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace com.es.si1213.trabalho2.sgpf.business.SQLMethods
{
    public class EmitirParecerComissaoProjectos : Settings
    {

        public static object getListaDeProjectosParaParecerComissaoProjecto(string Papel, string Username)
        {
            return data.Projecto.getListaDeProjectosParaParecerComissaoProjecto(Papel, Username);
        }

        public static void parecerComissaoProjecto(int NrProjecto, string Username, bool parecerFavoravel, string observacoes)
        {

            string param1 = "@Numero";
            string param2 = "@User";
            string param3 = "@Parecer";
            string param4 = "@Observacoes";

            int campoParam1 = NrProjecto;
            string campoParam2 = Username;
            int campoParam3;
            string campoParam4 = observacoes;

            campoParam3 = (parecerFavoravel.Equals(true) ? campoParam3 = 1 : campoParam3 = 0);

            connection = new SqlConnection(GetConnectionString());
            command = new SqlCommand(SQLQueryParecerComissaoFinanceira, connection);

            try
            {
                using (connection)
                {
                    connection.Open();

                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue(param1, campoParam1);
                    command.Parameters.AddWithValue(param2, campoParam2);
                    command.Parameters.AddWithValue(param3, campoParam3);
                    command.Parameters.AddWithValue(param4, campoParam4);
                    command.ExecuteNonQuery();
                }

                command.Dispose();
            }
            catch (Exception)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
            }

        }
    }
}
