﻿<%@ Page Title="SGPF" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="AccessDenied.aspx.cs" Inherits="com.es.si1213.trabalho2.sgpf.ui._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Acesso não autorizado
    </h2>
    <p>
        O seu papel, <b><%= Roles.GetRolesForUser()[0] %></b>, não tem permissões para aceder a esta página.
    </p>
</asp:Content>
