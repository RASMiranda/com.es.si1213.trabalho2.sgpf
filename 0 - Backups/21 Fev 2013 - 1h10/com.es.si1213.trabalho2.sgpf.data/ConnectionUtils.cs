﻿
using System.Configuration;
using System.Data.SqlClient;
using System.Data;


namespace com.es.si1213.trabalho2.sgpf.data
{
    public class ConnectionUtils
    {

        // SQL variables
        protected static SqlConnection connection;
        protected static SqlCommand command;
        protected static DataTable dt;
        protected static SqlDataReader dr;



        public static void configConnectionString(object ConnectionStringSettingsSGPF)
        {
            data.AccessData.ConnectionStringSettingsSGPF = (ConnectionStringSettings)ConnectionStringSettingsSGPF;
        }



        // Obtém Connection String para a base de dados SGPF
        protected static string GetConnectionString()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionStringSGPF"].ConnectionString;
        }



        // --- SQL Queries --- //
        // Verifica nr do último projecto na DB
        protected static string SQLQueryObtemNrUltimoProjecto = 
            "SELECT COUNT(*) FROM Projecto";


        // Abre Projecto
        protected static string SQLQueryAbreProjecto = 
            "INSERT INTO dbo.Projecto (NrProjecto, NomeProjecto, MontanteSolicitado, AutorProjecto, " +
            "StatusAprovacaoTecnico, UsernameTecnico, ObservacoesTecnico, ProjectoArquivado, ProjectoSuspenso, StatusAprovGestorFinanciamento, " +
            "StatusAprovComissaoFinanceira, DataParecerTecnico) VALUES (@Item1, @Item2, @Item3, @Item4, @Item5, @Item6, @Item7, 0, 0, 0, 0, @Item8)";


        // Arquiva Projecto
        protected static string SQLQueryArquivaProjecto = 
            "UPDATE dbo.Projecto SET ProjectoArquivado='1', ObservacoesProjectoArquivado=@Observacoes, UsernameArquivador=@User, " +
            "DataProjectoArquivado=@Data WHERE ([NrProjecto]=@Numero)";


        // Lista de Projectos para reactivar
        protected static string SQLQueryListaProjectosParaReactivar = 
            "SELECT [NrProjecto], [NomeProjecto], [MontanteSolicitado], [AutorProjecto], [StatusAprovacaoTecnico], " +
            "[DataParecerTecnico], [ObservacoesTecnico], [UsernameTecnico], [StatusAprovGestorFinanciamento], " +
            "[DataParecerGestorFinanciamento], [UsernameGestorFinanciamento], [ObservacoesGestorFinanciamento], " +
            "[StatusAprovComissaoFinanceira], [DataParecerComissaoFinanceira], [UsernameComissaoFinanceira], " +
            "[ObservacoesComissaoFinanceira] FROM [Projecto] WHERE ([ProjectoSuspenso]='1' AND [ProjectoArquivado]='0')";


        // Reactivar Projecto
        protected static string SQLQueryReactivarProjecto = 
            "UPDATE dbo.Projecto SET ProjectoSuspenso='0', UsernameProjectoReactivadoPor=@User, ObservacoesProjectoReactivado=@Observacoes " +
            "WHERE ([NrProjecto]=@Numero)";


        // Suspender Projecto
        protected static string SQLQuerySuspendeProjecto = 
            "UPDATE dbo.Projecto SET ProjectoSuspenso='1', ObservacoesProjectoSuspenso=@Observacoes " +
            "WHERE ([NrProjecto]=@Numero)";


        // Lista de Projectos para Parecer do Gestor
        protected static string SQLQueryListaParecerGestor = 
            "SELECT [NrProjecto], [NomeProjecto], [MontanteSolicitado], [AutorProjecto], [StatusAprovacaoTecnico], " +
            "[DataParecerTecnico], [ObservacoesTecnico], [UsernameTecnico], [StatusAprovGestorFinanciamento], " +
            "[DataParecerGestorFinanciamento], [UsernameGestorFinanciamento], [ObservacoesGestorFinanciamento], " +
            "[StatusAprovComissaoFinanceira], [DataParecerComissaoFinanceira], [UsernameComissaoFinanceira], " +
            "[ObservacoesComissaoFinanceira] FROM [Projecto] WHERE ([StatusAprovGestorFinanciamento]='0' AND [UsernameGestorFinanciamento]=@User " +
            "AND [ProjectoArquivado]='0')";


        // Parecer Gestor de Projecto
        protected static string SQLQueryParecerGestorProjecto = 
            "UPDATE dbo.Projecto SET StatusAprovGestorFinanciamento=@Parecer, UsernameGestorFinanciamento=@User, " +
            "ObservacoesGestorFinanciamento=@Observacoes, DataParecerGestorFinanciamento=@Data WHERE ([NrProjecto]=@Numero)";


        // Lista de Projectos para Parecer Comissao
        protected static string SQLQueryListaParecerComissao = 
            "SELECT [NrProjecto], [NomeProjecto], [MontanteSolicitado], [AutorProjecto], [StatusAprovacaoTecnico], " +
            "[DataParecerTecnico], [ObservacoesTecnico], [UsernameTecnico], [StatusAprovGestorFinanciamento], " +
            "[DataParecerGestorFinanciamento], [UsernameGestorFinanciamento], [ObservacoesGestorFinanciamento], " +
            "[StatusAprovComissaoFinanceira], [DataParecerComissaoFinanceira], [UsernameComissaoFinanceira], " +
            "[ObservacoesComissaoFinanceira] FROM [Projecto] WHERE ([StatusAprovComissaoFinanceira]='0' AND [UsernameComissaoFinanceira]=@User " +
            "AND [ProjectoArquivado]='0')";


        // Parecer Comissao Financeira
        protected static string SQLQueryParecerComissaoFinanceira =
            "UPDATE dbo.Projecto SET StatusAprovComissaoFinanceira=@Parecer, ProjectoArquivado=@Arquivo, " +
            "UsernameComissaoFinanceira=@User, ObservacoesComissaoFinanceira=@Observacoes, " +
            "ProjectoIncentivos=@ProjectoIncentivos, InicioPeriodoBonif=@InicioBonif, FimPeriodoBonif=@FimBonif, " +
            "NrPrestacoes=@NrPrestacoes, Taxa=@taxa, MontanteMax=@MontanteMax, ContaID=@ContaID WHERE ([NrProjecto]=@Numero)";


        // Obtem Montante actual para Projecto
        protected static string SQLQueryObtemMontanteActualDeProjecto =
            "SELECT [MontanteSolicitado] FROM [Projecto] WHERE ([NrProjecto]=@Numero)";


        // Reforco Projecto
        protected static string SQLQueryReforcoProjecto = 
            "UPDATE dbo.Projecto SET ObservacoesReforcoProjecto=@Observacoes, UsernameProjectoReforcadoPor=@User, " +
            "MontanteSolicitado=@NovoMontante WHERE ([NrProjecto]=@Numero)";


        // Lista de Projectos para Parecer Comissao
        protected static string SQLQueryListaProjectosReforco = 
            "SELECT [NrProjecto], [NomeProjecto], [MontanteSolicitado], [AutorProjecto], [StatusAprovacaoTecnico], " +
            "[DataParecerTecnico], [ObservacoesTecnico], [UsernameTecnico], [StatusAprovGestorFinanciamento], " +
            "[DataParecerGestorFinanciamento], [UsernameGestorFinanciamento], [ObservacoesGestorFinanciamento], " +
            "[StatusAprovComissaoFinanceira], [DataParecerComissaoFinanceira], [UsernameComissaoFinanceira], " +
            "[ObservacoesComissaoFinanceira] FROM [Projecto] WHERE ([UsernameProjectoReforcadoPor]=@User AND [ProjectoArquivado]='0')";
    }
}
