﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace com.es.si1213.trabalho2.sgpf.data.SQLMethods
{
    public class ReforcoFinanciamento : ConnectionUtils
    {

        public static object getListaDeProjectosParaReforcar(string Papel, string Username)
        {
            return data.Projecto.getListaDeProjectosParaReforcar(Papel, Username);
        }



        public static decimal ObtemMontanteProjecto(int NrProjecto)
        {
            var montanteActual = new int();

            string param1 = "@Numero";
            int campoParam1 = NrProjecto;

            connection = new SqlConnection(GetConnectionString());
            command = new SqlCommand(SQLQueryObtemMontanteActualDeProjecto, connection);

            try
            {
                using (connection)
                {
                    connection.Open();
                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue(param1, campoParam1);
                    dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        montanteActual = (int)(dr["MontanteSolicitado"]);
                    }
                }

                command.Dispose();
                dr.Dispose();
            }


            catch (Exception ex)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de ligação à base de dados');</script>");       
            }

            return montanteActual;
        }


        public static void reforcoProjecto(int NrProjecto, string Username, int Montante, string observacoes)
        {
            decimal valorActual = ObtemMontanteProjecto(NrProjecto);

            string param1 = "@Numero";
            string param2 = "@User";
            string param3 = "@NovoMontante";
            string param4 = "@Observacoes";

            
            int campoParam1 = NrProjecto;
            string campoParam2 = Username;
            decimal campoParam3 = (valorActual + Montante);
            string campoParam4 = observacoes;

            connection = new SqlConnection(GetConnectionString());
            command = new SqlCommand(SQLQueryReforcoProjecto, connection);

            try
            {
                using (connection)
                {
                    connection.Open();

                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue(param1, campoParam1);
                    command.Parameters.AddWithValue(param2, campoParam2);
                    command.Parameters.AddWithValue(param3, campoParam3);
                    command.Parameters.AddWithValue(param4, campoParam4);
                    command.ExecuteNonQuery();
                }

                command.Dispose();
            }


            catch (Exception)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
            }

        }
    }
}
