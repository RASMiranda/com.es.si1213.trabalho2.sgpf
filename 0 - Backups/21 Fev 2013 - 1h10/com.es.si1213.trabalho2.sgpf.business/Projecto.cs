﻿using System;
using System.Data;
using System.Data.SqlClient;
using com.es.si1213.trabalho2.sgpf.data;

//using com.es.si1213.trabalho2.sgpf.data;

namespace com.es.si1213.trabalho2.sgpf.business
{


    public class Projecto : ConnectionUtils
    {
        private int numProjecto;

        public int NumProjecto
        {
            get { return numProjecto; }
            set { numProjecto = value; }
        }
        private string nomeProjecto;

        public string NomeProjecto
        {
            get { return nomeProjecto; }
            set { nomeProjecto = value; }
        }
        private decimal montanteSolicitado;

        public decimal MontanteSolicitado
        {
            get { return montanteSolicitado; }
            set { montanteSolicitado = value; }
        }
        private string solicitadorProjecto;

        public string SolicitadorProjecto
        {
            get { return solicitadorProjecto; }
            set { solicitadorProjecto = value; }
        }
        private DateTime dataAbertura;

        public DateTime DataAbertura
        {
            get { return dataAbertura; }
            set { dataAbertura = value; }
        }
        private Boolean parecerTecnicoFavoravel;

        public Boolean ParecerTecnicoFavoravel
        {
            get { return parecerTecnicoFavoravel; }
            set { parecerTecnicoFavoravel = value; }
        }
        private string usernameTecnico;

        public string UsernameTecnico
        {
            get { return usernameTecnico; }
            set { usernameTecnico = value; }
        }


        public static string Abrir(
                         string NomeProjecto
                        , decimal MontanteSolicitado
                        , string SolicitadorProjecto
                        , Boolean ParecerTecnicoFavoravel
                        , string UsernameTecnico
                        , string Observacoes
                        , DateTime data)
        {


            return (AbrirProjecto.AbrirNovoProjecto(NomeProjecto
                         , MontanteSolicitado
                         , SolicitadorProjecto
                         , ParecerTecnicoFavoravel
                         , UsernameTecnico
                         , Observacoes
                         , data));
        }

      
    }
}
