﻿using System;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace com.es.si1213.trabalho2.sgpf.ui
{
    public partial class _EmitirParecer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GridView1.DataSource = data.EmitirParecerGestor.getListaDeProjectosParaParecerGestorProjecto(Roles.GetAllRoles()[0], User.Identity.Name);
            GridView1.DataBind();
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DataFilter1.BeginFilter();
            }
        }

        void DataFilter1_OnFilterAdded()
        {
            DataFilter1.FilterSessionID = "EmitirParecer.aspx";
            DataFilter1.FilterDataSource();
            GridView1.DataBind();
        }

        protected void btnEmitirParecer_click(object sender, EventArgs e)
        {
            this.FailureText.Visible = false;
            if (GridView1.SelectedIndex == -1)
            {
                this.FailureText.Visible = true;
                this.FailureText.Text = "Selecione um Projecto por favor.";
                return;
            }

            Int32 NrProjecto = (Int32)GridView1.SelectedDataKey.Value;
            string observavoes = this.txtObservacoes.Text;

            bool parecerFavoravel = false;
            if (this.rdBtParecer.SelectedIndex == 0)
                parecerFavoravel = true;

            data.EmitirParecerGestor.parecerGestorProjecto(NrProjecto, User.Identity.Name, parecerFavoravel, observavoes);

            this.txtObservacoes.Text = "";
            this.rdBtParecer.SelectedIndex = 0;
            
            string strFavoravel = "não favorável";;

            if (parecerFavoravel.Equals(true)) strFavoravel = "favorável";

     
            // Mensagem de Parecer favorável/não favorável
            System.Web.HttpContext.Current.Response.Write("<script>alert('Ao Projecto número " + NrProjecto.ToString() + 
                " obteve parecer " + strFavoravel + "!');</script>");
            Page_Load(null, EventArgs.Empty); // Provoca um Refresh á Gridview
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }
    }
}
