﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace com.es.si1213.trabalho2.sgpf.data
{
    public class EmitirParecerComissaoProjectos : ConnectionUtils
    {

        public static object getListaDeProjectosParaParecerComissaoProjecto(string Papel, string Username)
        {
            return data.Projecto.getListaDeProjectosParaParecerComissaoProjecto(Papel, Username);
        }

        public static void parecerComissaoProjecto(int NrProjecto, string Username, bool parecerFavoravel, string observacoes, 
            bool projectoIncentivos, DateTime inicioPeriodoBonif, DateTime fimPeriodoBonif,decimal nrPrestacoes, 
            decimal taxa, decimal montanteMax, string contaID)
        {

            string param1 = "@Numero";
            string param2 = "@User";
            string param3 = "@Parecer";
            string param4 = "@Arquivo";
            string param5 = "@Observacoes";
            string param6 = "@ProjectoIncentivos";
            string param7 = "@InicioBonif";
            string param8 = "@FimBonif";
            string param9 = "@NrPrestacoes";
            string param10 = "@taxa";
            string param11 = "@MontanteMax";
            string param12 = "@ContaID";

            int campoParam1 = NrProjecto;
            string campoParam2 = Username;
            int campoParam3;
            int campoParam4;
            string campoParam5 = observacoes;
            bool campoParam6 = projectoIncentivos;
            DateTime campoParam7 = inicioPeriodoBonif;
            DateTime campoParam8 = fimPeriodoBonif;
            decimal campoParam9 = nrPrestacoes;
            decimal campoParam10 = taxa;
            decimal campoParam11 = montanteMax;
            string campoParam12;

            campoParam3 = (parecerFavoravel.Equals(true) ? campoParam3 = 1 : campoParam3 = 0);
            campoParam4 = (parecerFavoravel.Equals(true) ? campoParam4 = 0 : campoParam4 = 1); // Arquiva se parecer for não favorável
            campoParam12 = (projectoIncentivos.Equals(true) ? campoParam12 = "" : campoParam12 = contaID); // Se Projecto de incentivos não existe conta bancária


            if (parecerFavoravel.Equals(false))
            {
                campoParam6 = false;
                campoParam7 = DateTime.Today;
                campoParam8 = DateTime.Today;
                campoParam9 = 0;
                campoParam10 = 0;
                campoParam11 = 0;
                campoParam12 = "";
            }


            connection = new SqlConnection(GetConnectionString());
            command = new SqlCommand(SQLQueryParecerComissaoFinanceira, connection);

            try
            {
                using (connection)
                {
                    connection.Open();

                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue(param1, campoParam1);
                    command.Parameters.AddWithValue(param2, campoParam2);
                    command.Parameters.AddWithValue(param3, campoParam3);
                    command.Parameters.AddWithValue(param4, campoParam4);
                    command.Parameters.AddWithValue(param5, campoParam5);
                    command.Parameters.AddWithValue(param6, campoParam6);
                    command.Parameters.AddWithValue(param7, campoParam7);
                    command.Parameters.AddWithValue(param8, campoParam8);
                    command.Parameters.AddWithValue(param9, campoParam9);
                    command.Parameters.AddWithValue(param10, campoParam10);
                    command.Parameters.AddWithValue(param11, campoParam11);
                    command.Parameters.AddWithValue(param12, campoParam12);
                    command.ExecuteNonQuery();
                }

                command.Dispose();
            }
            catch (Exception ex)
            {
                //System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
                System.Web.HttpContext.Current.Response.Write(ex.ToString());
            }

        }
    }
}
