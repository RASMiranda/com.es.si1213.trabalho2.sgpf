﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;

namespace com.es.si1213.trabalho2.sgpf.data
{
    public class ArquivarProjecto : ConnectionUtils
    {
        public static object getListaDeProjectosParaArquivar(string Papel, string Username)
        {
            return data.Projecto.getListaDeProjectosParaArquivar(Papel, Username);
        }

        public static void arquivarProjecto(int NrProjecto, string Username, string observacoes)
        {
            string param1 = "@Numero";
            string param2 = "@User";
            string param3 = "@Observacoes";
            string param4 = "@Data";
            int campoParam1 = NrProjecto;
            string campoParam2 = Username;
            string campoParam3 = observacoes;
            DateTime campoParam4 = DateTime.Today;

            connection = new SqlConnection(GetConnectionString());
            command = new SqlCommand(SQLQueryArquivaProjecto, connection);

            try
            {
                using (connection)
                {
                    connection.Open();

                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue(param1, campoParam1);
                    command.Parameters.AddWithValue(param2, campoParam2);
                    command.Parameters.AddWithValue(param3, campoParam3);
                    command.Parameters.AddWithValue(param4, campoParam4);
                    command.ExecuteNonQuery();
                }

                command.Dispose();
            }
            catch (Exception)
            {
                HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
            }

        }
    }
}
