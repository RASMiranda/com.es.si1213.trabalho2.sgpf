﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace com.es.si1213.trabalho2.sgpf.data
{
    public class EmitirParecerGestor : ConnectionUtils
    {
        public static object getListaDeProjectosParaParecerGestorProjecto(string Papel, string Username)
        {
            return data.Projecto.getListaDeProjectosParaParecerGestorProjecto(Papel, Username);
        }

        public static void parecerGestorProjecto(int NrProjecto, string Username, bool parecerFavoravel, string observacoes)
        {

            string param1 = "@Numero";
            string param2 = "@User";
            string param3 = "@Parecer";
            string param4 = "@Observacoes";
            string param5 = "@Data";

            int campoParam1 = NrProjecto;
            string campoParam2 = Username;
            int campoParam3 = (parecerFavoravel.Equals(true) ? campoParam3 = 1 : campoParam3 = 0);
            string campoParam4 = observacoes;
            DateTime campoParam5 = DateTime.Today;

            connection = new SqlConnection(GetConnectionString());
            command = new SqlCommand(SQLQueryParecerGestorProjecto, connection);

            try
            {
                using (connection)
                {
                    connection.Open();

                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue(param1, campoParam1);
                    command.Parameters.AddWithValue(param2, campoParam2);
                    command.Parameters.AddWithValue(param3, campoParam3);
                    command.Parameters.AddWithValue(param4, campoParam4);
                    command.Parameters.AddWithValue(param5, campoParam5);
                    command.ExecuteNonQuery();
                }

                command.Dispose();
            }
            catch (Exception ex)
            {
                //System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
                System.Web.HttpContext.Current.Response.Write(ex.ToString());
            }

        }
    }
}
