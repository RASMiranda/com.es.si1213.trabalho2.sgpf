﻿<%@ Page Title="SGPF" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="AbrirProjecto.aspx.cs" Inherits="com.es.si1213.trabalho2.sgpf.ui._AbrirProjecto" %>
<%@ Register Assembly="SandTrap.WebControls" Namespace="SandTrap.WebControls" TagPrefix="st" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .bigTextEntry
        {}
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Abrertura de Projecto
    </h2>
    <span class="failureNotification">
        <asp:Literal ID="FailureText" runat="server"></asp:Literal>
    </span>
    <asp:ValidationSummary ID="AbrirProjectoValidationSummary" runat="server" CssClass="failureNotification"
        ValidationGroup="AbrirProjectoValidationGroup" />
    <div class="divInfoProjecto">
        <fieldset class="fldsAbrirProjecto">
            <legend>Informação do Projecto</legend>
            <%--Número de Projecto--%>
            <p>
                <asp:Label ID="LblNumProjecto" runat="server" AssociatedControlID="txtNumProjecto">Número de Projecto:</asp:Label>
                <asp:TextBox ID="txtNumProjecto" runat="server" CssClass="textOutput" ReadOnly="true"></asp:TextBox>
            </p>
            <p>
            <%--Nome do Projecto--%>
                <asp:Label ID="lblNomeProjecto" runat="server" AssociatedControlID="txtNomeProjecto">Nome de Projecto:</asp:Label>
                <asp:TextBox ID="txtNomeProjecto" runat="server" CssClass="textEntry"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqNomeProjecto" runat="server" ControlToValidate="txtNomeProjecto"
                    CssClass="failureNotification" ErrorMessage="Nome do Projecto é obrigatório."
                    ToolTip="Nome do Projecto é obrigatório." ValidationGroup="AbrirProjectoValidationGroup">*</asp:RequiredFieldValidator>
            </p>
            <%--Montante Solicitado--%>
            <p>
                <asp:Label ID="lblMontante" runat="server" AssociatedControlID="currTxtMontante">Montante Solicitado:</asp:Label>
                <st:CurrencyBox ID="currTxtMontante" runat="server" CssClass="textEntry"></st:CurrencyBox>
            </p>
            <%--Projecto Solicitado por (Nome)--%>
            <p>
                <asp:Label ID="lblSolicitadoPor" runat="server" AssociatedControlID="txtSolicitadoPor">Projecto Solicitado por (Nome):</asp:Label>
                <asp:TextBox ID="txtSolicitadoPor" runat="server" CssClass="textEntry"></asp:TextBox>
                <asp:RequiredFieldValidator ID="reqSolicitadoPor" runat="server" ControlToValidate="txtSolicitadoPor"
                    CssClass="failureNotification" ErrorMessage="Solicitador do Projecto é obrigatório."
                    ToolTip="Solicitador do Projecto é obrigatório." ValidationGroup="AbrirProjectoValidationGroup">*</asp:RequiredFieldValidator>
            </p>
            <%--Parecer do técnico--%>
            <p>
                <asp:Label ID="lblParecer" runat="server" AssociatedControlID="rdBtParecer">Parecer do técnico:</asp:Label>
                <asp:RequiredFieldValidator ID="reqParecer" runat="server" ControlToValidate="rdBtParecer"
                    CssClass="failureNotification" ErrorMessage="Parecer é obrigatório."
                    ToolTip="Parecer é obrigatório." ValidationGroup="AbrirProjectoValidationGroup">*</asp:RequiredFieldValidator>
                <asp:RadioButtonList ID="rdBtParecer" runat="server">
                    <asp:ListItem>Favorável</asp:ListItem>
                    <asp:ListItem>Não favorável</asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <%--Observações--%>
            <p><asp:Label ID="lblObservacoes" runat="server" AssociatedControlID="txtObservacoes">Observações:</asp:Label></p>
            <p>
                <asp:TextBox ID="txtObservacoes" AcceptsTab=true AcceptsReturn=true 
                    ScrollBars="ScrollBars.Vertical" TextMode="multiline" runat="server" 
                    CssClass="bigTextEntry" Height="80px" Width="300px"></asp:TextBox>
            </p>
        </fieldset>
        <p>
            <asp:Button class="submitButton" ID="btnAbrirProjecto" runat="server" Text="Abrir Projecto" 
                ValidationGroup="AbrirProjectoValidationGroup" 
                onclick="btnAbrirProjecto_click" />
        </p>
    </div>
</asp:Content>
