﻿using System;
using System.Web.Security;
using System.Web.UI.WebControls;

namespace com.es.si1213.trabalho2.sgpf.ui
{
    public partial class _ReativarProjecto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //TODO: Uncomment//DataFilter1.DataSource = business.Projecto.getListaDeProjectosParaReactivar(Roles.GetAllRoles()[0], User.Identity.Name);
            //DataFilter1.DataSource = business.SQLMethods.ReactivarProjecto.getListaDeProjectosParaReactivar();
            ////GridView1.DataSource = business.SQLMethods.ReactivarProjecto.getListaDeProjectosParaReactivar();
            //DataFilter1.DataColumns = GridView1.Columns;
            //DataFilter1.FilterSessionID = "ReativarProjecto.aspx";
            //DataFilter1.OnFilterAdded += new DataFilter.RefreshDataGridView(DataFilter1_OnFilterAdded);

             //data.Projecto.getListaDeProjectosParare (Papel, Username);

            GridView1.DataSource = data.ReactivarProjecto.getListaDeProjectosParaReactivar(Roles.GetAllRoles()[0], User.Identity.Name);
            GridView1.DataBind();
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DataFilter1.BeginFilter();
            }
        }

        void DataFilter1_OnFilterAdded()
        {
            DataFilter1.FilterSessionID = "ReativarProjecto.aspx";
            DataFilter1.FilterDataSource();
            GridView1.DataBind();
        }

        protected void btnReactivar_click(object sender, EventArgs e)
        {
            this.FailureText.Visible = false;
            if (GridView1.SelectedIndex == -1)
            {
                this.FailureText.Visible = true;
                this.FailureText.Text = "Selecione um Projecto por favor.";
                return;
            }

            Int32 NrProjecto = (Int32)GridView1.SelectedDataKey.Value;
            string observacoes = this.txtObservacoes.Text;

            //business.Projecto.reactivarProjecto(NrProjecto, User.Identity.Name, observacoes);//, Roles.GetAllRoles()[0], User.Identity.Name, Obeservavoes);
            data.ReactivarProjecto.reactivarProjecto(NrProjecto, User.Identity.Name, observacoes);

            this.txtObservacoes.Text = "";

            // Mensagem de projecto Reactivado
            System.Web.HttpContext.Current.Response.Write("<script>alert('Projecto número " + NrProjecto.ToString() + " reactivado!');</script>");
            Page_Load(null, EventArgs.Empty); // Provoca um Refresh á Gridview
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }
    }
}
