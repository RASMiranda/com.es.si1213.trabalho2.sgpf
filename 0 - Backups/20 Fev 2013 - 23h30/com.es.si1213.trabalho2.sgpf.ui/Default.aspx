﻿<%@ Page Title="SGPF" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="com.es.si1213.trabalho2.sgpf.ui._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Bem-vindo ao SGPF!
    </h2>
    <p>
        Este é um sistema propriatário da organização XYZ.
        
    </p>
    <% if (!User.Identity.IsAuthenticated){ %>
    <p>
        Efectue o seu <a href="~/Account/Login.aspx" ID="HeadLoginStatus" runat="server">login</a> por favor.
    </p>
    <% } else{ %>
    <p>
        Sistema de gestão de projectos de financiamento para apoio à criação de redes de investigação e desenvolvimento (I&D)
    </p>
    <% } %>
</asp:Content>
