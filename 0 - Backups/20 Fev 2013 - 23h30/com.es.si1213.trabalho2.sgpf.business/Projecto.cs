﻿using System;
using System.Data;
using System.Data.SqlClient;
using com.es.si1213.trabalho2.sgpf.data;

//using com.es.si1213.trabalho2.sgpf.data;

namespace com.es.si1213.trabalho2.sgpf.business
{


    public class Projecto : ConnectionUtils
    {
        private int numProjecto;

        public int NumProjecto
        {
            get { return numProjecto; }
            set { numProjecto = value; }
        }
        private string nomeProjecto;

        public string NomeProjecto
        {
            get { return nomeProjecto; }
            set { nomeProjecto = value; }
        }
        private decimal montanteSolicitado;

        public decimal MontanteSolicitado
        {
            get { return montanteSolicitado; }
            set { montanteSolicitado = value; }
        }
        private string solicitadorProjecto;

        public string SolicitadorProjecto
        {
            get { return solicitadorProjecto; }
            set { solicitadorProjecto = value; }
        }
        private DateTime dataAbertura;

        public DateTime DataAbertura
        {
            get { return dataAbertura; }
            set { dataAbertura = value; }
        }
        private Boolean parecerTecnicoFavoravel;

        public Boolean ParecerTecnicoFavoravel
        {
            get { return parecerTecnicoFavoravel; }
            set { parecerTecnicoFavoravel = value; }
        }
        private string usernameTecnico;

        public string UsernameTecnico
        {
            get { return usernameTecnico; }
            set { usernameTecnico = value; }
        }


       



        public static string Abrir(
                         string NomeProjecto
                        , decimal MontanteSolicitado
                        , string SolicitadorProjecto
                        , Boolean ParecerTecnicoFavoravel
                        , string UsernameTecnico
                        , string Observacoes
                        , DateTime data)
        {

           
            ////return (AbrirNovoProjecto(NomeProjecto
            //return (AbrirProjecto.AbrirNovoProjecto(NomeProjecto
            //             , MontanteSolicitado
            //             , SolicitadorProjecto
            //             , ParecerTecnicoFavoravel
            //             , UsernameTecnico
            //             , Observacoes
            //             , data));
            //return (AbrirNovoProjecto(NomeProjecto
            return (AbrirProjecto.AbrirNovoProjecto(NomeProjecto
                         , MontanteSolicitado
                         , SolicitadorProjecto
                         , ParecerTecnicoFavoravel
                         , UsernameTecnico
                         , Observacoes
                         , data));
        }
   

       

        //public static void parecerGestorProjecto(int NrProjecto, bool parecerFavoravel, string Username, string observacoes)
        //{

        //    string param1 = "@Numero";
        //    string param2 = "@Parecer";
        //    string param3 = "@User";
        //    string param4 = "@Observacoes";

        //    int campoParam1 = NrProjecto;
        //    int campoParam2;
        //    string campoParam3 = Username;
        //    string campoParam4 = observacoes;
           
        //    campoParam2 = (parecerFavoravel.Equals(true)? campoParam2 = 1: campoParam2 = 0);

        //    connection = new SqlConnection(GetConnectionString());
        //    command = new SqlCommand(SQLQueryParecerGestorProjecto, connection);

        //    try
        //    {
        //        using (connection)
        //        {
        //            connection.Open();

        //            command.CommandType = CommandType.Text;
        //            command.Parameters.AddWithValue(param1, campoParam1);
        //            command.Parameters.AddWithValue(param2, campoParam2);
        //            command.Parameters.AddWithValue(param3, campoParam3);
        //            command.Parameters.AddWithValue(param4, campoParam4);
        //            command.ExecuteNonQuery();
        //        }

        //        command.Dispose();
        //    }
        //    catch (Exception)
        //    {
        //        System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
        //    }

        //}

        //public static object getListaDeProjectosParecerComissao(string Username)
        //{

        //    string param1 = "@User";
        //    string campoParam1 = Username;

        //    connection = new SqlConnection(GetConnectionString());
        //    command = new SqlCommand(SQLQueryListaParecerComissao, connection);
        //    dt = new DataTable();

        //    try
        //    {
        //        using (connection)
        //        {
        //            connection.Open();
        //            command.CommandType = CommandType.Text;
        //            command.Parameters.AddWithValue(param1, campoParam1);
        //            dt = data.AccessData.ExecuteReader(command);
        //        }

        //        command.Dispose();
        //    }


        //    catch (Exception)
        //    {
        //        System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
        //    }

        //    return dt;
        //}

      

        public static void reforcoProjecto(int NrProjecto, string Username, decimal Montante, string observacoes)
        {
            decimal montanteActual = 0;

            connection = new SqlConnection(GetConnectionString());
            command = new SqlCommand(SQLQueryObtemMontanteActualDeProjecto, connection);

            try
            {
                using (connection)
                {
                    connection.Open();
                    command.CommandType = CommandType.Text;
                    dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        montanteActual = (decimal)(dr["MontanteSolicitado"]);
                    }
                }

                command.Dispose();
            }


            catch (Exception)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
            }

            string param1 = "@Numero";
            string param2 = "@User";
            string param3 = "@NovoMontante";
            string param4 = "@Observacoes";


            int campoParam1 = NrProjecto;
            string campoParam2 = Username;
            decimal campoParam3 =  montanteActual + Montante;
            string campoParam4 = observacoes;

            connection = new SqlConnection(GetConnectionString());
            command = new SqlCommand(SQLQueryReforcoProjecto, connection);

            try
            {
                using (connection)
                {
                    connection.Open();

                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue(param1, campoParam1);
                    command.Parameters.AddWithValue(param2, campoParam2);
                    command.Parameters.AddWithValue(param3, campoParam3);
                    command.Parameters.AddWithValue(param4, campoParam4);
                    command.ExecuteNonQuery();
                }

                command.Dispose();
            }


            catch (Exception)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
            }

        }

        public static object getListaDeProjectosReforco(string Username)
        {

            string param1 = "@User";
            string campoParam1 = Username;

            connection = new SqlConnection(GetConnectionString());
            command = new SqlCommand(SQLQueryListaProjectosReforco, connection);
            dt = new DataTable();

            try
            {
                using (connection)
                {
                    connection.Open();
                    command.CommandType = CommandType.Text;
                    command.Parameters.AddWithValue(param1, campoParam1);
                    dt = data.AccessData.ExecuteReader(command);
                }

                command.Dispose();
            }


            catch (Exception)
            {
                System.Web.HttpContext.Current.Response.Write("<script>alert('Erro de inserção na base de dados');</script>");
            }

            return dt;
        }

        public static void parecerComissaoProjecto(int NrProjecto, string Username, bool parecerFavoravel, string Obeservavoes, bool projectoIncentivos, DateTime PeriodoBonifDe, DateTime PeriodoBonifAte, int numPrestacoes, decimal taxa, decimal montanteMax, string contaBancID)
        {

           // Quem é a comissão de Projecto?
           // O que é para fazer aqui ?

            //TODO.Miguel: business.Projecto.parecerComissaoProjecto
            throw new NotImplementedException();
        }
    }
}
