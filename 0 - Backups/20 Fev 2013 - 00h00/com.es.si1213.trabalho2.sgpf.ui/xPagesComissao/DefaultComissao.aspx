﻿<%@ Page Title="SGPF" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="DefaultComissao.aspx.cs" Inherits="com.es.si1213.trabalho2.sgpf.ui._DefaultComissao" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Opções da comissão financeira 
    </h2>
    <br />
    <asp:Menu ID="NavigationMenu" runat="server" EnableViewState="false" IncludeStyleBlock="false" Orientation="Vertical">
        <Items>
            <asp:MenuItem NavigateUrl="~/xPagesComissao/ReforcoFinanciamento.aspx" Text="Reforço de Financiamento"/>
            <asp:MenuItem NavigateUrl="~/xPagesComuns/ReativarProjecto.aspx" Text="Reativar Projecto"/>
            <asp:MenuItem NavigateUrl="~/xPagesComuns/ArquivarProjecto.aspx" Text="Arquivar Projecto"/>
            <asp:MenuItem NavigateUrl="~/xPagesComissao/EmitirParecerComissao.aspx" Text="Emitir Parecer"/>
            <asp:MenuItem NavigateUrl="~/xPagesComuns/SuspenderProjecto.aspx" Text="Suspender Projecto"/>
        </Items>
    </asp:Menu>
</asp:Content>
