﻿<%@ Page Title="SGPF" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="EmitirParecer.aspx.cs" Inherits="com.es.si1213.trabalho2.sgpf.ui._EmitirParecer" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="~\xControls\DataFilter.ascx" TagName="DataFilter" TagPrefix="uc1" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Emissão de Parecer sobre o Projecto
    </h2>
    <h3>
        Selecione o Projecto:
    </h3>
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <%--TODO: Fix DataFilter--%><uc1:DataFilter ID="DataFilter1" runat="server" />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div style="overflow-x: auto; width: 100%; overflow-y: auto; height: 100%">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" EmptyDataText="Não existem projectos carentes de parecer"
                        DataKeyNames="NrProjecto" GridLines="None" AllowPaging="true" CssClass="mGrid"
                        PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" PageSize="5" OnPageIndexChanging="GridView1_PageIndexChanging"
                        AutoGenerateSelectButton="True">
                        <AlternatingRowStyle CssClass="alt" />
                        <Columns>
                            <asp:BoundField DataField="NrProjecto" HeaderText="Nº Projecto" ReadOnly="True" SortExpression="NrProjecto" />
                            <asp:BoundField DataField="NomeProjecto" HeaderText="Nome Projecto" SortExpression="NomeProjecto" />
                            <asp:BoundField DataField="MontanteSolicitado" HeaderText="Montante" SortExpression="MontanteSolicitado"
                                DataFormatString="{0:C}" />
                            <asp:BoundField DataField="AutorProjecto" HeaderText="Autor" SortExpression="AutorProjecto" />
                            <%--TODO: Field Status type/formating?--%>
                            <asp:BoundField DataField="StatusAprovacaoTecnico" HeaderText="Parecer Técnico" SortExpression="StatusAprovacaoTecnico" />
                            <asp:BoundField DataField="DataParecerTecnico" HeaderText="Data Parecer Técnico"
                                SortExpression="DataParecerTecnico" DataFormatString=" {0:d}" />
                            <asp:BoundField DataField="ObeservacoesTecnico" HeaderText="Obeservações Técnico"
                                SortExpression="ObeservacoesTecnico" />
                            <asp:BoundField DataField="UsernameTecnico" HeaderText="Utilizador Tecnico" SortExpression="UsernameTecnico" />
                        </Columns>
                        <PagerStyle CssClass="pgr" />
                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <%--Parecer--%>
        <p>
            <asp:Label ID="lblParecer" runat="server" AssociatedControlID="rdBtParecer">Parecer:</asp:Label>
            <asp:RadioButtonList ID="rdBtParecer" runat="server">
                <asp:ListItem Selected="True">Favorável</asp:ListItem>
                <asp:ListItem>Não favorável</asp:ListItem>
            </asp:RadioButtonList>
        </p>
        <p>
        <%--Botão--%>
        <asp:Button ID="btnEmitirParecer" runat="server" Text="Emitir Parecer" class="submitButton"
            OnClick="btnEmitirParecer_click" />
        <span class="failureNotification">
            <asp:Literal ID="FailureText" runat="server"></asp:Literal>
        </span>
        </p>
        <%--Observações--%>
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblObservacoes" runat="server">Observações:</asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtObservacoes" AcceptsTab="true" AcceptsReturn="true" ScrollBars="ScrollBars.Vertical"
                        TextMode="multiline" runat="server" CssClass="bigTextEntry" Height="80px" Width="300px"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
