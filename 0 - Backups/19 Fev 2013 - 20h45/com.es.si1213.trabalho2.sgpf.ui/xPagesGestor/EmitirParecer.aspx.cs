﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.es.si1213.trabalho2.sgpf.ui.xControls;
using System.Web.Security;

namespace com.es.si1213.trabalho2.sgpf.ui
{
    public partial class _EmitirParecer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //TODO: Uncomment//DataFilter1.DataSource = business.Projecto.getListaDeProjectosParecerGestor(User.Identity.Name);
            DataFilter1.DataColumns = GridView1.Columns;
            DataFilter1.FilterSessionID = "EmitirParecer.aspx";
            DataFilter1.OnFilterAdded += new DataFilter.RefreshDataGridView(DataFilter1_OnFilterAdded);
            GridView1.DataBind();
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DataFilter1.BeginFilter();
            }
        }

        void DataFilter1_OnFilterAdded()
        {
            DataFilter1.FilterSessionID = "EmitirParecer.aspx";
            DataFilter1.FilterDataSource();
            GridView1.DataBind();
        }

        protected void btnEmitirParecer_click(object sender, EventArgs e)
        {
            this.FailureText.Visible = false;
            if (GridView1.SelectedIndex == -1)
            {
                this.FailureText.Visible = true;
                this.FailureText.Text = "Selecione um Projecto por favor.";
                return;
            }

            Int32 NrProjecto = (Int32)GridView1.SelectedDataKey.Value;
            string Obeservavoes = this.txtObservacoes.Text;

            bool parecerFavoravel = false;
            if (this.rdBtParecer.SelectedIndex == 0)
                parecerFavoravel = true;

            business.Projecto.parecerComissaoProjecto(NrProjecto, User.Identity.Name, parecerFavoravel, Obeservavoes);

            this.txtObservacoes.Text = "";
            this.rdBtParecer.SelectedIndex = 0;
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }
    }
}
