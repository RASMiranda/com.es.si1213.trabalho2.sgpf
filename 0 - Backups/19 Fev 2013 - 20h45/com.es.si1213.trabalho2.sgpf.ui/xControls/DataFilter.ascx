﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DataFilter.ascx.cs"
    Inherits="com.es.si1213.trabalho2.sgpf.ui.xControls.DataFilter" %>
<asp:UpdatePanel ID="updatePanel" runat="server">
    <contenttemplate>
        <asp:Panel ID="pnlNewFilter" runat="server"></asp:Panel>
        <asp:Panel ID="pnlToolbar" runat="server">
        <%--TODO: Fix DataFilter> btnAddNewFilter Visible true--%>
            <asp:Button Visible="False" ID="btnAddNewFilter" runat="server" OnClick="btnAddNewFilter_Click" Text="Adicionar Filtro" CssClass="submitButton" />
            <asp:Button ID="btnAndNewFilter" runat="server" CssClass="submitButton" Text="AND" OnClick="btnAndNewFilter_Click" Visible="False" />
            <asp:Button ID="btnOrNewFilter" runat="server" CssClass="submitButton" Text="OR" OnClick="btnOrNewFilter_Click" Visible="False" /></asp:Panel>
        </asp:Panel>
        <%--TODO: Fix DataFilter> Label Visible true--%>
        <asp:Label Visible="False" runat="server" Text="Label">(Prima 'Enter' para pesquisar)</asp:Label>
    </contenttemplate>
</asp:UpdatePanel>
