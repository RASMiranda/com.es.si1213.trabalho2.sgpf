﻿using System.Data.Common;
using System.Data;

namespace com.es.si1213.trabalho2.sgpf.data
{
    public class Projecto
    {
        // Obtem lista de Projectos para Arquivar
        public static DataTable getListaDeProjectosParaArquivar(string Papel, string Username)
        {
            using (DbCommand cmd = AccessData.CreateCommand())
            {
                //TODO: //cmd.CommandText = "SELECT * FROM obterProjectosArquivar(@Papel,@Username)"; //TODO: Add parameters
                cmd.CommandText = "SELECT [NrProjecto], [NomeProjecto], [MontanteSolicitado], [AutorProjecto], [StatusAprovacaoTecnico], " +
                  "[DataParecerTecnico], [ObservacoesTecnico], [UsernameTecnico], [StatusAprovGestorFinanciamento], " +
                  "[DataParecerGestorFinanciamento], [UsernameGestorFinanciamento], [ObservacoesGestorFinanciamento], " +
                  "[StatusAprovComissaoFinanceira], [DataParecerComissaoFinanceira], [UsernameComissaoFinanceira], " +
                  "[ObservacoesComissaoFinanceira] FROM [Projecto] WHERE ([ProjectoArquivado]='0')";
                cmd.CommandType = CommandType.Text;
                return AccessData.ExecuteReader(cmd);
 
            }
        }

        // Obtem lista de Projectos para Reactivar
        public static DataTable getListaDeProjectosParaReactivar(string Papel, string Username)
        {
            using (DbCommand cmd = AccessData.CreateCommand())
            {
                cmd.CommandText = "SELECT [NrProjecto], [NomeProjecto], [MontanteSolicitado], [AutorProjecto], [StatusAprovacaoTecnico], " +
                  "[DataParecerTecnico], [ObservacoesTecnico], [UsernameTecnico], [StatusAprovGestorFinanciamento], " +
                  "[DataParecerGestorFinanciamento], [UsernameGestorFinanciamento], [ObservacoesGestorFinanciamento], " +
                  "[StatusAprovComissaoFinanceira], [DataParecerComissaoFinanceira], [UsernameComissaoFinanceira], " +
                  "[ObservacoesComissaoFinanceira], [DataProjectoArquivado], [PapelArquivador], [UsernameArquivador], " +
                  "[ObservacoesProjectoArquivado] FROM [Projecto] WHERE ([ProjectoSuspenso]='1' AND [ProjectoArquivado]='0')";
                cmd.CommandType = CommandType.Text;
                return AccessData.ExecuteReader(cmd);
            }
        }


        // Obtem lista de Projectos para Suspender
        public static DataTable getListaDeProjectosParaSuspender(string Papel, string Username)
        {
            using (DbCommand cmd = AccessData.CreateCommand())
            {
                cmd.CommandText = "SELECT [NrProjecto], [NomeProjecto], [MontanteSolicitado], [AutorProjecto], [StatusAprovacaoTecnico], " +
                  "[DataParecerTecnico], [ObservacoesTecnico], [UsernameTecnico], [StatusAprovGestorFinanciamento], " +
                  "[DataParecerGestorFinanciamento], [UsernameGestorFinanciamento], [ObservacoesGestorFinanciamento], " +
                  "[StatusAprovComissaoFinanceira], [DataParecerComissaoFinanceira], [UsernameComissaoFinanceira], " +
                  "[ObservacoesComissaoFinanceira], [DataProjectoArquivado], [PapelArquivador], [UsernameArquivador], " +
                  "[ObservacoesProjectoArquivado] FROM [Projecto] WHERE ([ProjectoSuspenso]='0' AND [ProjectoArquivado]='0')";
                cmd.CommandType = CommandType.Text;
                return AccessData.ExecuteReader(cmd);
            }
        }

        // Obtem lista de Projectos para Parecer Gestor de Projectos
        public static DataTable getListaDeProjectosParaParecerGestorProjecto(string Papel, string Username)
        {
            using (DbCommand cmd = AccessData.CreateCommand())
            {
                cmd.CommandText = "SELECT [NrProjecto], [NomeProjecto], [MontanteSolicitado], [AutorProjecto], [StatusAprovacaoTecnico], " +
                  "[DataParecerTecnico], [ObservacoesTecnico], [UsernameTecnico], [StatusAprovGestorFinanciamento], " +
                  "[DataParecerGestorFinanciamento], [UsernameGestorFinanciamento], [ObservacoesGestorFinanciamento], " +
                  "[StatusAprovComissaoFinanceira], [DataParecerComissaoFinanceira], [UsernameComissaoFinanceira], " +
                  "[ObservacoesComissaoFinanceira], [DataProjectoArquivado], [PapelArquivador], [UsernameArquivador], " +
                  "[ObservacoesProjectoArquivado] FROM [Projecto] WHERE ([StatusAprovGestorFinanciamento]='0' AND [StatusAprovacaoTecnico]='1' " +
                  "AND [ProjectoArquivado]='0')";
                cmd.CommandType = CommandType.Text;
                return AccessData.ExecuteReader(cmd);
            }
        }

        // Obtem lista de Projectos para Parecer da Comissão de Projectos
        public static DataTable getListaDeProjectosParaParecerComissaoProjecto(string Papel, string Username)
        {
            using (DbCommand cmd = AccessData.CreateCommand())
            {
                cmd.CommandText = "SELECT [NrProjecto], [NomeProjecto], [MontanteSolicitado], [AutorProjecto], [StatusAprovacaoTecnico], " +
                  "[DataParecerTecnico], [ObservacoesTecnico], [UsernameTecnico], [StatusAprovGestorFinanciamento], " +
                  "[DataParecerGestorFinanciamento], [UsernameGestorFinanciamento], [ObservacoesGestorFinanciamento], " +
                  "[StatusAprovComissaoFinanceira], [DataParecerComissaoFinanceira], [UsernameComissaoFinanceira], " +
                  "[ObservacoesComissaoFinanceira], [DataProjectoArquivado], [PapelArquivador], [UsernameArquivador], " +
                  "[ObservacoesProjectoArquivado] FROM [Projecto] WHERE ([StatusAprovComissaoFinanceira]='0' AND [StatusAprovacaoTecnico]='1' " +
                  "AND [StatusAprovGestorFinanciamento]='1' AND [ProjectoArquivado]='0')";
                cmd.CommandType = CommandType.Text;
                return AccessData.ExecuteReader(cmd);
            }
        }

        // Obtem lista de Projectos para Parecer Reforçar
        public static DataTable getListaDeProjectosParaReforcar(string Papel, string Username)
        {
            using (DbCommand cmd = AccessData.CreateCommand())
            {
                cmd.CommandText = "SELECT [NrProjecto], [NomeProjecto], [MontanteSolicitado], [AutorProjecto], [StatusAprovacaoTecnico], " +
                  "[DataParecerTecnico], [ObservacoesTecnico], [UsernameTecnico], [StatusAprovGestorFinanciamento], " +
                  "[DataParecerGestorFinanciamento], [UsernameGestorFinanciamento], [ObservacoesGestorFinanciamento], " +
                  "[StatusAprovComissaoFinanceira], [DataParecerComissaoFinanceira], [UsernameComissaoFinanceira], " +
                  "[ObservacoesComissaoFinanceira], [DataProjectoArquivado], [PapelArquivador], [UsernameArquivador], " +
                  "[ObservacoesProjectoArquivado] FROM [Projecto] WHERE ([StatusAprovComissaoFinanceira]='1' AND [StatusAprovacaoTecnico]='1' " +
                  "AND [StatusAprovGestorFinanciamento]='1' AND [ProjectoArquivado]='0' AND [ProjectoSuspenso]='0')";
                cmd.CommandType = CommandType.Text;
                return AccessData.ExecuteReader(cmd);
            }
        }
    }
}
