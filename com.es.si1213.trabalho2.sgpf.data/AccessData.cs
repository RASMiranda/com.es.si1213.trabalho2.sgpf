﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Configuration;
using System.Data;

namespace com.es.si1213.trabalho2.sgpf.data
{
    public static class AccessData
    {
        public static ConnectionStringSettings ConnectionStringSettingsSGPF;

        static AccessData()
        {
        }

        //Cria um commando
        public static DbCommand CreateCommand()
        {
            string dbProviderName = ConnectionStringSettingsSGPF.ProviderName;
            string dbConnectionString = ConnectionStringSettingsSGPF.ConnectionString;
            DbProviderFactory factory = DbProviderFactories.GetFactory(dbProviderName);
            DbConnection connection = factory.CreateConnection();
            connection.ConnectionString = dbConnectionString;
            DbCommand command = connection.CreateCommand();
            return command;
        }

        public static DataTable ExecuteReader(DbCommand command)
        {
            DataTable table;
            try
            {
                command.Connection.Open();
                DbDataReader reader = command.ExecuteReader();
                table = new DataTable();
                table.Load(reader);
            }
            catch (Exception ex)
            { throw ex; }
            finally
            { command.Connection.Close(); }
            return table;
        }
  
        public static int ExecuteNoneQuery(DbCommand command)
        {
            int rowsChanged = -1;
            try
            {
                command.Connection.Open();
                rowsChanged = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            { throw ex; }
            finally
            { command.Connection.Close(); }
            return rowsChanged;
        }

        public static object ExecuteScalar(DbCommand command)
        {
            object value = "";
            try
            {
                command.Connection.Open();
                value = command.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            { throw ex; }
            finally
            { command.Connection.Close(); }
            return value;
        }

    }
}
